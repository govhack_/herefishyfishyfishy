#!/home/cptblack/python/Python-3.5.2/python

SPECIES_PATH = "species.txt"
LIFEFORM_PATH = "all_lifeforms.txt"

# debuggan'
import cgitb, cgi, json, random, urllib.request, urllib.parse
cgitb.enable()

# Read query parameters
form = cgi.FieldStorage()
n = 10
if "n" in form:
	n = int(form["n"])
selectionType = "random"
if "selection" in form:
	selectionType = form["selection"]
lifeForm = None
if "lifeForm" in form:
	lifeForm = form["lifeForm"]

# Load species list
if not lifeForm:
	f = open(SPECIES_PATH, "rU")
	allSpecies = json.loads(f.read())
	f.close()
else:
	f = open(LIFEFORM_PATH, "rU")
	allSpecies = json.loads(f.read())[lifeForm]
	f.close(0)

if selectionType == "first":
	species = allSpecies[:n]
else:
	species = random.sample(allSpecies, n)

out = []
for sp in species:
	# Get locations
	html = urllib.request.urlopen(
		"http://cptblack.cf/herefishyfishyfishy/get_loc.py?species=" + \
		urllib.parse.quote(sp)
	).read().decode("utf-8")
	html = html.replace("'",'"')
	trueLocs = json.loads(html)

	out.append(
		{"name": sp,
		"locations": trueLocs}
	)

# Write out response
print("Content-Type: text/plain\n")
print(json.dumps(out))