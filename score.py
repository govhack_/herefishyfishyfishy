#!/home/cptblack/python/Python-3.5.2/python

CACHE_PATH = "location_cache.txt"
AUSTRALIA = 4000/2 # Did you know that Aus is ~4000km across?

# enable debugging
import cgitb
cgitb.enable()

import cgi, urllib.request, urllib.parse, json, os

# Helper function: LatLng distance.
from math import cos, asin, sqrt
def latLngDistance(lat1, lon1, lat2, lon2):
    p = 0.017453292519943295
    a = 0.5 - cos((lat2 - lat1) * p)/2 + cos(lat1 * p) * cos(lat2 * p) * (1 - cos((lon2 - lon1) * p)) / 2
    return 12742 * asin(sqrt(a))

# Read the query parameters.
form = cgi.FieldStorage()

species = "Eucalyptus%20camaldulensis%20subsp.%20arida"
if "species" in form:
	species = str(form["species"])
	species = species.split("'")[3]
	species = urllib.parse.quote(species)

# print("Content-Type: text/plain\n")
# print(species)
# exit()

lat = -23.96
lng = 133.40
if "lat" in form and "lng" in form:
	try:
		lat = float(form["lat"])
		lng = float(form["lng"])
	except Exception:
		lat = float(form.getvalue("lat"))
		lng = float(form.getvalue("lng"))

html = urllib.request.urlopen(
	"http://cptblack.cf/herefishyfishyfishy/get_loc.py?species=" + \
	species
).read().decode("utf-8")
html = html.replace("'",'"')
trueLocs = json.loads(html)

# calculate score
distance = float("inf")
for loc in trueLocs:
	distance = min(
		distance,
		latLngDistance(lat, lng, loc["lat"], loc["lng"])
	)
inAustralias = distance / AUSTRALIA
score = 0
if inAustralias < 1:
	score = int(round(10 * (1 - inAustralias), 0))

# calculate color
if score < 4:
	color = "#FF0000"
elif score < 6:
	color = "rgb(255, 117, 0)"
elif score < 8:
	color = "#FFBB00"
else:
	color = "rgb(19, 159, 19)"

# Output the response as "Yes" or "No"
print("Content-Type: text/plain\n")
print(json.dumps({"score":score, "color":color}))