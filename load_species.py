# Not a web app!
import urllib.request, json

f = open("species.txt", "w")

for pageNum in range(30114):
	# Load page
	requestUrl = "https://api.aekos.org.au/v1/allSpeciesData.json" + \
		"?rows=%d&start=%d" % (20, pageNum)

	data = json.loads(urllib.request.urlopen(requestUrl).read().decode("utf-8"))
	rows = data["response"]
	# extract scientific names only
	names = [row["scientificName"] for row in rows]

	f.write(json.dumps(names)[1:-1] + ",")

	print("Up to page %d" % pageNum)
	if pageNum % 10 == 0:
		print("Flushin'")
		f.flush()

f.close()