# NOT A WEB APP/SERVICE!
# Makes calls to get_loc.py to cause pre-caching of species locations.

import json, urllib.request

SPECIES_PATH = "species.txt"

f = open(SPECIES_PATH, "rU")
allSpecies = json.loads(f.read())

for i, sp in enumerate(allSpecies):
	print("Loading species %d of %d" % (i, len(allSpecies)))

	# Get locations
	html = urllib.request.urlopen(
		"http://cptblack.cf/herefishyfishyfishy/get_loc.py?species=" + \
		urllib.parse.quote(sp)
	).read()
	# get_loc.py will automatically cache the result if 
	# it isn't already cached.

	# Also get lifeform.
	html = urllib.request.urlopen(
		"http://cptblack.cf/herefishyfishyfishy/get_lifeform.py?species=" + \
		urllib.parse.quote(sp)
	).read()


print("Done!")