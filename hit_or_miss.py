#!/home/cptblack/python/Python-3.5.2/python

CACHE_PATH = "hit_or_miss_cache.txt"

# enable debugging
import cgitb
cgitb.enable()

import cgi, urllib.request, json, os

# Helper function: LatLng distance.
from math import cos, asin, sqrt
def latLngDistance(lat1, lon1, lat2, lon2):
    p = 0.017453292519943295
    a = 0.5 - cos((lat2 - lat1) * p)/2 + cos(lat1 * p) * cos(lat2 * p) * (1 - cos((lon2 - lon1) * p)) / 2
    return 12742 * asin(sqrt(a))


# Read the query parameters.
form = cgi.FieldStorage()
if "species" in form:
	species = form["species"]
else:
	species = "Eucalyptus%20camaldulensis%20subsp.%20arida"
if "tolerance" in form:
	tolerance = float(form["tolerance"]) # in kilometres
else:
	tolerance = 100
if "lat" in form and "lng" in form:
	lat = float(form["lat"])
	lng = float(form["lng"])
else:
	lat = -23.96
	lng = 133.40

# Check if the species is cached
f = open(CACHE_PATH, "rU")
cache = json.loads(f.read())
f.close()
trueLocs = []
if species in cache:
	trueLocs = cache[species]
	trueLat, trueLng = trueLocs[0]
else:
	# Find the official latlng for the species. (TODO: allow multiple latlngs)
	requestUrl = "https://api.aekos.org.au/v1/traitData?speciesName=" + \
		species + "&rows=1"
	html = urllib.request.urlopen(requestUrl).read().decode("utf-8")

	# This is JSON.
	jsonData = json.loads(html)
	#print("Content-Type: text/plain\n\n")
	print(jsonData)
	trueLat = jsonData["response"][0]["decimalLatitude"]
	trueLng = jsonData["response"][0]["decimalLongitude"]
	trueLocs = [(trueLat, trueLng)]

	# Since it wasn't cached, we should cache this.
	cache[species] = trueLocs
	f = open(CACHE_PATH, "w")
	f.write(json.dumps(cache))
	f.close()

distance = latLngDistance(lat, lng, trueLat, trueLng)
result = True
if distance > tolerance:
	result = False

# Output the response as True or False
print("Content-Type: text/plain\n")
print(result)