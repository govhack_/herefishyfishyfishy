#!/home/cptblack/python/Python-3.5.2/python

CACHE_PATH = "lifeforms_cache.txt"
ALL_LIFEFORMS = "all_lifeforms.txt"

import cgitb, cgi, json, random, urllib.request, urllib.parse
cgitb.enable()

# Read query parameters
form = cgi.FieldStorage()
species = "Eucalyptus%20camaldulensis%20subsp.%20arida"
if "species" in form:
	species = str(form["species"])
	species = species.split("'")[3]
	species = urllib.parse.quote(species)

# Get cached response if possible
f = open(CACHE_PATH, "rU")
lifeFormCache = json.loads(f.read())
f.close()

if species in lifeFormCache:
	lifeForm = lifeFormCache[species]
else:
	# Not in cache; gotta load it
	html = urllib.request.urlopen(
		"https://api.aekos.org.au/v1/traitData?speciesName=" + \
		species + "&traitName=lifeForm&rows=1"
	).read().decode("utf-8")
	lifeForm = json.loads(html)["response"][0]["traits"][0]["value"]

	# add to cache
	lifeFormCache[species] = lifeForm
	f = open(CACHE_PATH, "w")
	f.write(json.dumps(lifeFormCache))
	f.close()

	# Also make sure this lifeform is in the list of all lifeforms
	f = open(ALL_LIFEFORMS,"rU")
	data = json.loads(f.read())
	f.close()
	if lifeForm not in data:
		data[lifeForm] = []
	data[lifeForm].append(species)

	f = open(ALL_LIFEFORMS, "w")
	f.write(json.dumps(data))
	f.close()

print("Content-Type: text/plain\n")
print(lifeForm)
