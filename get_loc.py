#!/home/cptblack/python/Python-3.5.2/python

import cgitb
cgitb.enable()

from collections import Counter
import json,cgi,urllib.request

CACHE_PATH = "location_cache.txt"

# Read query parameters
form = cgi.FieldStorage()
species = "Eucalyptus%20camaldulensis%20subsp.%20arida"
if "species" in form:
	species = str(form["species"])
	species = species.split("'")[3]
	species = urllib.parse.quote(species)

# Check if the species location is cached
f = open(CACHE_PATH, "rU")
cache = json.loads(f.read())
f.close()
trueLocs = []

#print("Content-Type: text/plain\n")
#print(species)

if species in cache:
	trueLocs = cache[species]
else:
	# Find the official latlngs for the species. (first 500, at least)
	requestUrl = "https://api.aekos.org.au/v1/traitData?speciesName=" + \
		species + "&rows=500"
	html = urllib.request.urlopen(requestUrl).read().decode("utf-8")

	# This is JSON.
	jsonData = json.loads(html)

	trueLocs = [(x["decimalLatitude"], x["decimalLongitude"]) for x in
		jsonData["response"]]
	counter = Counter(trueLocs)
	trueLocs = [
		{"lat":item[0][0], "lng":item[0][1], "weight":item[1]}
		for item in counter.items()
	]

	# Since it wasn't cached, we should cache this.
	cache[species] = trueLocs
	f = open(CACHE_PATH, "w")
	f.write(json.dumps(cache))
	f.close()

print("Content-Type: text/plain\n")
print(cache[species])