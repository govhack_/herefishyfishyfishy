

var currentQuestion = 0;
var totalQuestions = 10;
var currentScore = 0;

var heatmapDuration = 3000;
var heatmapVisible = false;

var species;

var map, heatmap;

var scoreMessages = ["Way off!", "Too bad", "Too bad", "A bit off", "A bit off", "Close", "Close", "Very close!", "Very close!", "Great job!", "Great job!" ];


function initMap() {
	// Create a map object and specify the DOM element for display.
	map = new google.maps.Map(document.getElementById('map'), {
		center: {lat: -27.68352808378776, lng: 133.8134765625},
		scrollwheel: false,
		draggable: false,
		zoom: 5,
		mapTypeControl: false,
		streetViewControl: false,
		zoomControl: false,
		disableDoubleClickZoom: true
	});


	map.addListener('click', function(e) {
	    console.log('lat: ' + e.latLng.lat() + '  lng: ' + e.latLng.lng());

	    // Do nothing if the game is complete
	    if(currentQuestion >= totalQuestions || heatmapVisible) {
	    	return;
	    }

	    var speciesDiv = $('#species-list div.species').eq(currentQuestion);
	    var speciesName = speciesDiv.text();
	    var encodedSpeciesName = encodeURIComponent(speciesName);

	    // TODO Use the service for this
	    //var correct = Math.floor(Math.random() * 2) == 0;

	    $.get('../score.py?species=' + encodedSpeciesName + '&lat=' + e.latLng.lat() + '&lng=' + e.latLng.lng(), function(data) {
	    	data = $.parseJSON(data);
	    	console.log('score returned');
	    	console.log(data);

	    	currentScore += parseInt(data.score);
		    updateScore();

		    $('.score-counter').text('+' + data.score);
		    $('.score-counter').css('bottom', '110px').css('opacity', '1.0');
		    $('.score-counter').show();
		    $('.score-counter').animate( { 'bottom': '180px', 'opacity': '0.0'}, 800);

		    speciesDiv.css('background-color', data.color);

		    $('.thisScoreContainer .message').text(scoreMessages[parseInt(data.score)]);
		    $('.thisScoreContainer').show();

		    showHeatmap();
		    setTimeout(function() {
		    	$('.thisScoreContainer').fadeOut(500, function() {

		    	});

		    }, 1000);


		    
		    

	    });

	 //    if(correct) {
	 //    	speciesDiv.addClass('correct');
	 //    	currentScore++;
	 //    }
	 //    else {
	 //    	speciesDiv.addClass('incorrect');
	 //    }


	});
}


var showHeatmap = function() {
	var heatmapData = [];

    //console.log('species: ', species[currentQuestion]);

    for(var index=0; index<species[currentQuestion].locations.length; index++) {
    	var location = species[currentQuestion].locations[index];

    	var sighting = {};
    	sighting.location = new google.maps.LatLng(location.lat, location.lng);
    	sighting.weight = parseInt(location.weight);

    	heatmapData.push(sighting);
    }

	var heatmap = new google.maps.visualization.HeatmapLayer({
	  data: heatmapData
	});
	heatmap.setMap(map);
	heatmap.set('radius', 15);
	
	var gradient = [
      'rgba(0, 255, 255, 0)',
      'rgba(0, 255, 255, 1)',
      'rgba(0, 191, 255, 1)',
      'rgba(0, 127, 255, 1)',
      'rgba(0, 63, 255, 1)',
      'rgba(0, 0, 255, 1)',
      'rgba(0, 0, 223, 1)',
      'rgba(0, 0, 191, 1)',
      'rgba(0, 0, 159, 1)',
      'rgba(0, 0, 127, 1)',
      'rgba(63, 0, 91, 1)',
      'rgba(127, 0, 63, 1)',
      'rgba(191, 0, 31, 1)',
      'rgba(255, 0, 0, 1)'
    ]
    heatmap.set('gradient', gradient);
    heatmapVisible = true;

	setTimeout(function() {

		heatmap.setMap(null);
		heatmapVisible = false;
		$('#speciesImageContainer').hide();

		currentQuestion++;

	    // Highlight new species and change photo
		highlightCurrentSpecies(currentQuestion);

	    if(currentQuestion >= totalQuestions) {
	    	// If that was the last question then show the congrats message
	    	$('.congratulationsMessage').show();
	    }
	    else {
	    	// Otherwise load up the photo for the next species
			var nextSpeciesDiv = $('#species-list div.species').eq(currentQuestion);
			flickrSearchOptions["text"] = nextSpeciesDiv.text();
			makeFlickrRequest(flickrSearchOptions, flickrRequestCallback);
	    }

	}, heatmapDuration);

};


var flickrSearchOptions = { 
	"api_key": "adfafe5952d9833c29b9e73c452983e1",
	"method": "flickr.photos.search",
	"format": "json",
	"nojsoncallback": "1",
	"per_page": "10",
	"text": "koala"
}

var makeFlickrRequest = function(options, cb) {
	var url, item, first;

	url = "https://api.flickr.com/services/rest/";
	first = true;
	$.each(options, function(key, value) { 
		url += (first ? "?" : "&") + key + "=" + value;
		first = false; 
	});

	$.get(url, function(data) { cb(data); });

};

var flickrRequestCallback = function(data) {
	//console.log('flickr data:');
	//console.log(data);

	if(data.photos.photo.length) {
		var imageData = data.photos.photo[0];

		// Generate image url
		var imageUrl = 'https://farm' + imageData.farm + '.staticflickr.com/' + imageData.server + '/' + imageData.id + '_' + imageData.secret + '.jpg'; 

		$('#speciesImage').attr('src', imageUrl);
		$('#speciesImage').on('load', function() {
			$('#speciesImageContainer').show();
		});
		
	}	
}

//makeFlickrRequest(flickrSearchOptions, flickrRequestCallback);

var populateSpeciesList = function() {

	// TODO replace this with a call to the service
	//var species = ["Eucalyptus baxteri", "Senna artemisioides subsp. oligophylla", "Eugenia lucida", "Triodia bitextura", "Callitris glaucophylla", "Casuarina pauper", "Schoenia ayersii", "Festuca stuartiana"];
	
	$.get('../get_species.py', function(data) {
		data = eval(data);

		console.log('data: ');
		console.log(data);

		species = data.sort(function(a, b) {
			if(a.locations.length < b.locations.length) {
				return true;
			}
			else {
				return false;
			}
		});

		totalQuestions = species.length;
		$('.totalQuestions').text(totalQuestions * 10);
		$('.score').show();

		for(var index=0; index<species.length; index++) {
			var thisSpecies = species[index];
			var html = '<div class="species"><a href="#">' + thisSpecies.name + '</a></div>';

			$('#species-list').append(html);
		}

		highlightCurrentSpecies(0);

		var firstSpecies = $('#species-list > div.species:first > a').text();
		console.log('first species: ' + firstSpecies);
		flickrSearchOptions["text"] = firstSpecies;
		makeFlickrRequest(flickrSearchOptions, flickrRequestCallback);

	});

}

var highlightCurrentSpecies = function(index) {
	console.log('highlight: ' + index);

	$('#species-list div.species').removeClass('active');
	$('#species-list div.species').eq(index).addClass('active');
}


$('#species-list').on('click', function(e) {
	e.preventDefault();

	alert('Please click on the position on the map where the species can be located');

	return false;
});

var updateScore = function() {
	$('.currentScore').text(currentScore);
}


$('a.try-again').on('click', function() {
	location.reload();
});

$('.introMessage .beginPlaying').on('click', function() {
	$('.introMessage').hide();
	populateSpeciesList();
});











